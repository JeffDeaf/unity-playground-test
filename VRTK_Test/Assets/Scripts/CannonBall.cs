﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    /// <summary>
    /// Livslängden i sekunder
    /// </summary>
    public float lifespan;
    
    //Den här funktionen utförs få fort en ny kanonkula skapas
    void Start()
    {
        /// Förstör den här kanonkulan efter en viss tid
        Destroy(gameObject, lifespan);
    }
}
